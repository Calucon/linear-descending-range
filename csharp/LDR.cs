using System;

namespace de.calucon.math
{
    public static class LDR
    {
        /// <summary>
        /// Generates a sequence of [range] linear descending numbers whose sum is [sum]
        /// </summary>
        /// <param name="range">Amount of numbers to generate</param>
        /// <param name="sum">total sum of all generated numbers, default 1.0</param>
        /// <returns></returns>
        public static double[] get(int range, double sum = 1.0)
        {
            if (range < 1) throw new ArgumentOutOfRangeException("[range] must be 1 or greater");

            double[] numbers = new double[range];
            double n = range + 1;
            double s = 1.0 / range;

            for(int i = 0; i < range; i++)
            {
                double x = i * s;
                numbers[i] = (-2.0 * x + 2.0) * sum / n;
            }

            return numbers;
        }

    }
}