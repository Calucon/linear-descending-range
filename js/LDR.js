/**
 * Generates a sequence of [range] linear descending numbers whose sum is [sum]
 * @param {int} range Amount of numbers to generate
 * @param {number} [sum=1.0] total sum of all generated numbers, default 1.0
 */
function ldr(range, sum){
    sum = (typeof sum !== 'undefined') ? sum : 1.0;
    sum = Number(sum);
    range = Number(range);
    if(range < 1) throw '[range] must be 1 or greater';

    var numbers = new Array(range);
    var n = range + 1;
    var s = 1.0 / range;

    for(var i = 0; i < range; i++){
        var x = i * s;
        numbers[i] = (-2.0*x + 2.0)*sum / n;
    }

    return numbers
}