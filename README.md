# Linear Descending Range
This function generates a series of `n` numbers.    
The generated numbers are sorted in a `descending` order.     
The `sum` of all numbers is `1.0` by default, but can be changed by the developer.     
Each number is by a constant value `x` smaller than it's predecessor.

> #### Potential Usage
> - Weighted Moving Average (WMA)
     
# Function
```
f(x) = a(-2x + 2.0) / n     
     
a: sum of all values     
n: value count + 1     

step size: x => i/range
```

# Implementations
- [x] Java
- [x] JavaScript
- [x] Kotlin
- [x] Python
- [ ] PHP
- [x] C#
- [x] SQL *(mssql only so far)*
- [ ] *other languages i haven't learned yet*
      
      
# Implementation as Filter
The filter implementation is still in progress and will be released soon.