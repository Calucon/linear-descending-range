package de.calucon.math;

final public class LDR {
    private LDR(){}

    /**
     * Generates a sequence of [range] linear descending numbers whose sum is 1.0
     * @param range Amount of numbers to generate
     */
    public static Double[] get(int range){
        return LDR.get(range, 1.0);
    }

    /**
     * Generates a sequence of [range] linear descending numbers whose sum is [sum]
     * @param range Amount of numbers to generate
     * @param sum total sum of all generated numbers, default 1.0
     */
    public static Double[] get(int range, double sum){
        if(range < 1) throw new IllegalArgumentException("[range] must be 1 or greater");

        Double[] numbers = new Double[range];
        double n = range + 1;
        double s = 1.0 / range;

        for(int i = 0; i < range; i++){
            double x = i * s;
            numbers[i] = (-2.0*x + 2.0)*sum / n;
        }

        return numbers;
    }
}