package de.calucon.math

/**
 * Generates a sequence of [range] linear descending numbers whose sum is [sum]
 * @param range Amount of numbers to generate
 * @param sum total sum of all generated numbers, default 1.0
 */
inline fun ldr(range: Int, sum: Double = 1.0): DoubleArray{
    if(range < 1) throw IllegalArgumentException("[range] must be 1 or greater")

    val numbers = DoubleArray(range)
    val n = range + 1
    val s = 1.0 / range

    for(i in 0 until range){
        val x = i * s
        numbers[i] = (-2.0*x + 2.0)*sum / n
    }

    return numbers
}