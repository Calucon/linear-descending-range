USE <database_name>;

-- =============================================
-- Author:      Simon Schwitz
-- Create date: 1/3/2019
-- Description: Generates a sequence of [range] linear descending numbers whose sum is [sum]
-- Parameters:
--   @range - Amount of numbers to generate (minimum 1)
--   @sum - total sum of all generated numbers, default 1.0
-- Returns:    Column containing all numbers n a descending order
-- =============================================

GO
CREATE PROCEDURE dbo.ldr @range INT, @sum FLOAT = 1.0
AS
	IF @range < 1 
		SET @range = 1
	
	DECLARE @numbers VARCHAR(MAX) = ''
	DECLARE @n FLOAT = @range + 1
	DECLARE @s FLOAT = 1.0 / @range
	DECLARE @i INT = 0

	DECLARE @x FLOAT = 0.0
	DECLARE @sr FLOAT = 0.0

	WHILE @i < @range
	BEGIN
		SET @x = @i * @s
		SET @sr = (-2.0*@x + 2.0)*@sum / @n
		SET @numbers = CONCAT(@numbers, '|', CONVERT(VARCHAR(MAX), @sr))
		SET @i = @i + 1
	END

	SET @numbers = RIGHT(@numbers, LEN(@numbers) - 1)
	SELECT CONVERT(float, value) AS number FROM STRING_SPLIT(@numbers, '|')
GO