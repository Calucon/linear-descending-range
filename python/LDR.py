def ldr(count=int, total=float):
    """
    Generates a sequence of [count] linear descending numbers whose sum is [total]
    
    ...

    Parameters
    ----------
    count : int
       Amount of numbers to generate
    total: float, optional
        total sum of all generated numbers, default 1.0
    """

    if not isinstance(count, int):
        raise ValueError('[count] must be an int!')
    if not isinstance(total, float):
        total = 1.0
    if count < 1:
        raise ValueError('[range] must be 1 or greater')

    numbers = list()
    n = count + 1.0
    s = 1.0 / count

    for i in range(count):
        x = i * s
        numbers.append((-2.0*x + 2.0)*total / n)

    return numbers